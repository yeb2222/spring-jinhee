package com.example.web;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.config.ApplicationProperties;

@Controller
public class HomeController {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    @SuppressWarnings("unused")
	private ApplicationProperties applicationProperties;
    
    private String activities;
    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    public HomeController(ApplicationProperties applicationProperties) {
    	this.applicationProperties = applicationProperties;
	}
    
    
	@GetMapping(value="/")
	@ResponseBody
	public String home(Model model) {

		Resource resource = resourceLoader.getResource("classpath:/activities.json");
		try {
			activities = IOUtils.toString(resource.getInputStream(),"utf-8");
			log.info("The preference.json was loaded successfully.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return activities;
	}
}
